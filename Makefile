include .env

.PHONY: up down stop prune ps logs

default: up

up:
	@echo "Starting up containers for $(COMPOSE_PROJECT_NAME)..."
	@docker-compose pull
	@docker-compose up -d --remove-orphans

down: stop

stop:
	@echo "Stopping containers for $(COMPOSE_PROJECT_NAME)..."
	@docker-compose stop

prune:
	@echo "Removing containers for $(COMPOSE_PROJECT_NAME)..."
	@docker-compose down -v --remove-orphans

ps:
	@docker ps --filter name='$(COMPOSE_PROJECT_NAME)*'


logs:
	@docker-compose logs -f $(filter-out $@,$(MAKECMDGOALS))

# https://stackoverflow.com/a/6273809/1826109
%:
	@: