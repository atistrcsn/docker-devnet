This creates a local docker netwrok and reverse-proxy for development.

## Installation
First add a new rule to your hosts to make use of .localhost TLD at development time:
```bash
# edit hosts file
$ sudo nano /etc/hosts

# Add the next line and save:
127.0.0.1       *.localhost
```
* start: `make up`
* stop: `make down` vagy `make stop`
* remove container: `make prune`
* see container logs live: `make logs`

See content of `Makefile` for more command.

## Usage
Set the following `labels` in your docker-compose service file:
```dockerfile
    labels:
      - 'traefik.port=${YOUR_SERVICE_PORT}'
      - 'traefik.backend=${YOUR_SERVICE_NAME}'
      - 'traefik.frontend.network=gateway'
      - 'traefik.frontend.rule=Host:example.localhost'
      #- 'traefik.frontend.auth.basic=user:$$secure$$password'
```

## Basic auth user:password generator
```bash
echo $(htpasswd -Bnb user password) | sed -e s/\\$/\\$\\$/g
```
> With bcrypt (secured) encoding and escaping.  
Do not use escaping in .env file.  

## App dashboards
* Traefik gateway: [traefik.localhost](http://traefik.localhost) - Documentation: [https://docs.traefik.io](https://docs.traefik.io)
* Container manager : [portainer.localhost](http://portainer.localhost/#/containers) - GitHub: [https://github.com/portainer/portainer](https://github.com/portainer/portainer)
* Mailhog mail catcher : [mailhog.localhost](http://mailhog.localhost/) - GitHub: [https://github.com/mailhog/MailHog](https://github.com/mailhog/MailHog)
